//
//  DogSoundboardViewController.m
//  DogSoundboard
//
//  Created by Andrew on 6/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DogSoundboardViewController.h"

@implementation DogSoundboardViewController

@synthesize button1, button2, button3, button4, button5, button6, button7, button8, button9, button10, button11, button12;
@synthesize player;
@synthesize bannerView_;

/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[super viewDidLoad];
	
	NSString *filePath = [[NSBundle mainBundle] pathForResource:@"dog-agh" ofType:@"wav"];  
	NSData *myData = [NSData dataWithContentsOfFile:filePath];  
	player =[[AVAudioPlayer alloc] initWithData:myData error:nil];
	[player prepareToPlay]; 
	player.numberOfLoops = 0;
	[player play];
	[player pause];
	
	bannerView_ = [[GADBannerView alloc]
						initWithFrame:CGRectMake(0.0, 
												 self.view.frame.size.height - GAD_SIZE_320x50.height, 
												 GAD_SIZE_320x50.width,
												 GAD_SIZE_320x50.height)];
	
	
	bannerView_.adUnitID = @"a14e1db8aadd92d";
	bannerView_.rootViewController = self;
	bannerView_.delegate = self;
	[self.view addSubview:bannerView_];
	bannerIsVisible = TRUE;
    
	[bannerView_ loadRequest:[GADRequest request]];

}

-(void)viewWillAppear:(BOOL)animated
{
	NSString *backgroundImage;
	NSLog(@"%f", [[UIScreen mainScreen] scale]);
    

//  Fill out code later to determine if iPhone 4 and display retina
//	if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.000000){
//		//iPhone 4
//        NSLog(@"We're an iPhone 4!");
//		 backgroundImage = @"dog_retina.png";
//	} else {
//        NSLog(@"We're an iPhone 3.2");
//		backgroundImage = @"dog_standard.png";
//	}

	backgroundImage = @"dog_standard.png";
	UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:backgroundImage]];
	self.view.backgroundColor = background;
	[background release];
	[backgroundImage release];
	
}



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
	[bannerView_ release];
}

-(IBAction)buttonPressed:(id)sender
{	
	UIButton *button = (UIButton*)sender;
	NSString *text = button.titleLabel.text;
	NSString *filename;
	
	if ([text isEqualToString:@"Aaaaahhh!"])
	{
		filename = @"dog-agh";
	} else if ([text isEqualToString:@"Aaaaoooowww!"])
	{
		filename = @"dog-ugh2";
	} else if ([text isEqualToString:@"Uggggghhh!"])
	{
		filename = @"dog-ugh";
	} else if ([text isEqualToString:@"You're Kiddin Me"])
	{
		filename = @"dog-kiddin";
	} else if ([text isEqualToString:@"The Maple Kind"])
	{
		filename = @"dog-maple1";
	} else if ([text isEqualToString:@"Maple Kind, Yeah"])
	{
		filename = @"dog-maple";
	} else if ([text isEqualToString:@"Covered With What?"])
	{
		filename = @"dog-cov";
	} else if ([text isEqualToString:@"What?"])
	{
		filename = @"dog-what";
	} else if ([text isEqualToString:@"What was in there?"])
	{
		filename = @"dog-whatwas";
	} else if ([text isEqualToString:@"Yeah? 1"])
	{
		filename = @"dog-yea";
	} else if ([text isEqualToString:@"Yeah? 2"])
	{
		filename = @"dog-yea2";
	} else if ([text isEqualToString:@"Watch Full Video"])
	{
		NSURL *url = [[NSURL alloc] initWithString:@"http://www.youtube.com/watch?v=nGeKSiCQkPw"];
		[[UIApplication sharedApplication] openURL:url];
	} 

	NSString *filePath = [[NSBundle mainBundle] pathForResource:filename ofType:@"wav"];  
	NSData *myData = [NSData dataWithContentsOfFile:filePath];
	[player stop];
	[player initWithData:myData error:nil];
	[player prepareToPlay];
	[player play];
	
}
- (void)bannerView:(GADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    if (bannerIsVisible)
    {
        [UIView beginAnimations:@"animateAdBannerOff" context:NULL];
        // Assumes the banner view is placed at the bottom of the screen.
        bannerView_.frame = CGRectOffset(bannerView_.frame, 0, bannerView_.frame.size.height);
        [UIView commitAnimations];
        bannerIsVisible = NO;
    }
}

-(IBAction)loadWebsite:(id)sender
{
	UIButton *temp = (UIButton*)sender;
	NSString *text = temp.titleLabel.text;
	
	if ([text isEqualToString:@"visit www.dysproseum.com"])
	{
		[[UIApplication sharedApplication] openURL:[[NSURL alloc] initWithString:@"http://www.dysproseum.com"]];
	} else {
		[[UIApplication sharedApplication] openURL:[[NSURL alloc] initWithString:@"http://www.codefortravel.com"]];
	}

}

- (void)dealloc {
	[button1 release];
	[button2 release];
	[button3 release];
	[button4 release];
	[button5 release];
	[button6 release];
	[button7 release];
	[button8 release];
	[button9 release];
	[button10 release];
	[button11 release];
	[button12 release];
	[player release];
    [bannerView_ release];
    [super dealloc];
}


@end
