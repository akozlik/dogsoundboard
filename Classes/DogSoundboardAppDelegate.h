//
//  DogSoundboardAppDelegate.h
//  DogSoundboard
//
//  Created by Andrew on 6/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DogSoundboardViewController;

@interface DogSoundboardAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    DogSoundboardViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet DogSoundboardViewController *viewController;

@end

