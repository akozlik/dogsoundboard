//
//  DogSoundboardViewController.h
//  DogSoundboard
//
//  Created by Andrew on 6/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "GADBannerView.h"


@interface DogSoundboardViewController : UIViewController <GADBannerViewDelegate> {
	UIButton *button1;
	UIButton *button2;
	UIButton *button3;
	UIButton *button4;
	UIButton *button5;
	UIButton *button6;
	UIButton *button7;
	UIButton *button8;
	UIButton *button9;
	UIButton *button10;
	UIButton *button11;
	UIButton *button12;
    
    BOOL bannerIsVisible;
	
	GADBannerView *bannerView_;
	
	AVAudioPlayer *player;
	
}

@property (nonatomic, retain) IBOutlet UIButton *button1;
@property (nonatomic, retain) IBOutlet UIButton *button2;
@property (nonatomic, retain) IBOutlet UIButton *button3;
@property (nonatomic, retain) IBOutlet UIButton *button4;
@property (nonatomic, retain) IBOutlet UIButton *button5;
@property (nonatomic, retain) IBOutlet UIButton *button6;
@property (nonatomic, retain) IBOutlet UIButton *button7;
@property (nonatomic, retain) IBOutlet UIButton *button8;
@property (nonatomic, retain) IBOutlet UIButton *button9;
@property (nonatomic, retain) IBOutlet UIButton *button10;
@property (nonatomic, retain) IBOutlet UIButton *button11;
@property (nonatomic, retain) IBOutlet UIButton *button12;

@property (nonatomic, retain) AVAudioPlayer *player;
@property (nonatomic, retain) GADBannerView *bannerView_;

-(IBAction) buttonPressed:(id)sender;
-(IBAction)loadWebsite:(id)sender;

@end

